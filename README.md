1/ How it work ?
This is very small system.
I will build API manager wallet.

2/ How to set up ? 

Docker.
Install Docker if you dont have in your laptop : https://docs.docker.com/docker-for-windows/install/

1/ You will have test_api_docker.tar file in setup folder. Make sure your Docker is installed. 
Open your terminal and import setup.rar to Docker : docker load -i test_api_docker.tar

2/ Find path of folder code in setup folder i sent. Ex: /Users/John/Downloads/test. In MacOs, you can go to this folder on terminal and try command : pwd.

3/ Change path in (2) to here : 
docker run -it -v <your_path_in_2>:<your_path_you_want_in_docker> --name <name_container> --network bridge -p <port of django run>:<port_on_your_laptop_can_connect> <IMage ID - you can use: docker images ,to find>

Ex: 
sudo docker run -it  -v /Users/quangtu/Documents/test:/home/dulieu --name test_project --network bridge -p 80:8000 f643c72bc252

4/ cd /home/dulieu/project001/
   . venv/bin/activate
   cd mini_wallet
   python3 manage.py runserver 0.0.0.0:8000

5/ username: root
   pass : P@ssw0rd

--------
TEST ON YOUR TERMINAL

# V1- POST Initialize my account for wallet
Success : 
curl --location --request POST 'http://localhost/api/v1/init/' \
--form 'customer_xid="ea0212d3-abd6-406f-8c67-86d8e814a3
"'

Fail:
curl --location --request POST 'http://localhost/api/v1/init/'

#  V2 - POST Enable my wallet         
Success : 
curl --location --request POST 'http://localhost/api/v1/wallet/' \
--header 'Authorization: Token b9f2bcbf0b20df4efeb37a00632d7d1fbd024603'

# V3 - GET View my wallet balance
Success : 
curl --location --request GET 'http://localhost/api/v1/wallet/' \
--header 'Authorization: Token b9f2bcbf0b20df4efeb37a00632d7d1fbd024603'


#v4 POST Add virtual money to my wallet
curl --location --request POST 'http://localhost/api/v1/wallet/deposits/' \
--header 'Authorization: Token b9f2bcbf0b20df4efeb37a00632d7d1fbd024603' \
--form 'amount="10000"' \
--form 'reference_id="ea0212dd3-abd6-4306f-8c67-868e814a3"'

#5 POST Use virtual money from my wallet
curl --location --request POST 'http://localhost/api/v1/wallet/withdrawals/' \
--header 'Authorization: Token b9f2bcbf0b20df4efeb37a00632d7d1fbd024603' \
--form 'amount="100"' \
--form 'reference_id="50dd53f2234d6-2-49229-8cc39-00reea06f52413sfdd4"'


#6 PATCH Disable my wallet
curl --location --request PATCH 'http://localhost/api/v1/wallet/' \
--header 'Authorization: Token b9f2bcbf0b20df4efeb37a00632d7d1fbd024603' \
--form 'is_disabled="true"'

---------------
UPDATE TEST UNIT

You can go to terminal in folder code : python3 manage.py test