from django.apps import AppConfig


class MiniWalletAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mini_wallet_app'
