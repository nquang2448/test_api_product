# Generated by Django 3.2.4 on 2021-06-22 09:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mini_wallet_app', '0002_accountwallet'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accountwallet',
            name='account',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mini_wallet_app.account', unique=True),
        ),
    ]
