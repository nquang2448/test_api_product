# Generated by Django 3.2.4 on 2021-06-22 19:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mini_wallet_app', '0012_alter_accountwallet_enabled_at'),
    ]

    operations = [
        migrations.CreateModel(
            name='Logging',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('balance_current', models.IntegerField()),
                ('balance_changed', models.IntegerField()),
                ('reference_id', models.CharField(max_length=42, unique=True)),
                ('changed_at', models.DateTimeField(blank=True, null=True)),
                ('owned_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mini_wallet_app.accountwallet', unique=True)),
            ],
        ),
    ]
