from django.test import TestCase
import unittest
from .models import Account
from .views import generate_key, CreateWalletForAccount
import json
from .methods import *

# Fake data 
customer_xid = 'ea0212d3-abd6-406f-8c67-868e814a2436'
token = '6b3f7dc70abe8aed3e56658b86fa508b472bf238'
reference_id = '50535246-dcb2-4929-8cc9-004ea06f5241'
class MethodsTest(TestCase):
    @classmethod
    def setUpTestData(self):
        CreateMyAccount_POST(customer_xid,token)
        print('account', Account.objects.get(customer_xid=customer_xid))
        print('token', Account.objects.get(token=token).token)
        print('wallet ', AccountWallet.objects.get(owned_by__customer_xid=customer_xid))
        print('Setup finished !')

    def test_CreateAccount_and_Wallet_POST(self):
        # CreateMyAccount_POST(customer_xid,token):
        CreateMyAccount_POST('aaa-abd6-406f-8c67-8814a2436','6b3f7dc70abe8aed3esdsda86fa508b472b3333')
        test_list = Account.objects.get(customer_xid ='aaa-abd6-406f-8c67-8814a2436')
        test_wallet_create = AccountWallet.objects.get(owned_by__customer_xid ='aaa-abd6-406f-8c67-8814a2436')
        # print('Account : ', test_list)
        # print('Wallet : ',test_wallet_create)
        if test_list and test_wallet_create:
            result = True
        else: 
            result = False
        self.assertEqual(result, True)

    def test_EnableWallet_POST(self):
        # EnableWallet_POST(token)
        before_status = AccountWallet.objects.get(owned_by__token = token).status
        EnableWallet_POST(token)
        after_status = AccountWallet.objects.get(owned_by__token = token).status
        # print('before_status ',before_status)
        # print('after_status : ',after_status)
        if before_status == False and after_status == True:
            result = True
        else: 
            result = False
        self.assertEqual(result, True)

    def test_EnableWallet_GET(self):
        # EnableWallet_GET(token):
        status_code = EnableWallet_GET(token).status_code
        self.assertEqual(status_code, 200)

    def test_EnableWallet_PATCH(self):
        # EnableWallet_PATCH(token, is_disabled_body)
        status_code = EnableWallet_PATCH(token, 'true').status_code
        self.assertEqual(status_code, 200)

    def test_WalletDeposit_POST(self):
        # WalletDeposit_POST(token,amount, reference_id)
        data1= AccountWallet.objects.get(owned_by__token=token).balance
        # Enable wallet
        EnableWallet_POST(token)
        # Deposit 
        WalletDeposit_POST(token,10000, reference_id)
        data = AccountWallet.objects.get(owned_by__token=token).balance
        # print('result', data)
        self.assertEqual(data, 10000)


    def test_UserMoney_POST(self):
        # UserMoney_POST(token, amount, reference_id)
        # Enable wallet
        EnableWallet_POST(token)
        # Deposit 
        WalletDeposit_POST(token,10000, '35246-dcb2-4929-8cc9-004ea06f524')
        # Use money
        UserMoney_POST(token, 1000, reference_id)
        data = AccountWallet.objects.get(owned_by__token=token).balance
        # print("result : ",data) #return 9000
        self.assertEqual(data, 9000)

if __name__ == '__main__':
    unittest.main()