from django.db import models
import binascii
import os
def generate_key():
    token_generate = binascii.hexlify(os.urandom(20)).decode()
    existed_token_check = Account.objects.filter(token=token_generate).count()
    # Check if had existed_token_check -> create new token
    while True:
        if existed_token_check > 0:
            token_generate = binascii.hexlify(os.urandom(20)).decode()
            existed_token_check = Account.objects.filter(token=token_generate).count()
        else:
            break
    return token_generate
class Account(models.Model):
    customer_xid = models.CharField(max_length=40,unique=True)
    token = models.CharField(max_length=42,unique=True)
    status = models.BooleanField(default=True)
    def __str__(self):
        return self.customer_xid


class AccountWallet(models.Model):
    owned_by = models.ForeignKey(Account,on_delete=models.CASCADE,unique=True)
    balance = models.IntegerField()
    status = models.BooleanField(default=False)
    enabled_at = models.DateTimeField(blank=True,null=True)
    def __str__(self):
        return self.owned_by.customer_xid

    
class Logging(models.Model):
    owned_by = models.ForeignKey(AccountWallet,on_delete=models.CASCADE)
    balance_current = models.IntegerField()
    balance_changed = models.IntegerField()
    reference_id = models.CharField(max_length=42,unique=True)
    deposited_at = models.DateTimeField(blank=True,null=True)
    def __str__(self):
        return self.owned_by.owned_by.customer_xid

    