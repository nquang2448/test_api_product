from django.shortcuts import render
from .models import Account, AccountWallet, Logging
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
import re
import datetime
from .methods import *

# V1- POST Initialize my account for wallet
@csrf_exempt
def CreateMyAccount(request):
    if request.is_ajax and request.method == "POST":
        # Check request have customer_xid or not
        if len(request.POST) == 1 and request.POST.get('customer_xid') != None:
            # Get data from method POST
            customer_xid = request.POST["customer_xid"]
            # Generated token for account
            token = generate_key()
            return CreateMyAccount_POST(customer_xid,token)
        else:
            return JsonResponse( {
                            "data": {
                                "error": {
                                "customer_xid": [
                                    "Missing data for required field."
                                ]
                                }
                            },
                            "status": "fail"
                        })     
    return JsonResponse({
                         "data": {
                            "detail": 'Not support this method'
                        },
                        "status": "error"       
    })

#  V2 - POST Enable my wallet       
@csrf_exempt
def EnableWallet(request):
    if request.is_ajax and request.method == "POST":
        # Check from headers of request have Authorization or not
        if request.headers.get('Authorization') != None:
            # Get data from method POST
            token = request.headers["Authorization"]
            token = token.replace("Token ","")
            return EnableWallet_POST(token) 
        else:
            return JsonResponse( {
                            "data": {
                                "error": {
                                "Authorization": [
                                    "Missing data for required field."
                                ]
                                }
                            },
                            "status": "fail"
                        })
#V3 - GET View my wallet balance
    elif request.is_ajax and request.method == "GET":
        if request.headers.get('Authorization') != None:
            token = request.headers["Authorization"]
            token = token.replace("Token ","")
            return EnableWallet_GET(token)
        else:
            return JsonResponse( {
                                "data": {
                                    "error": {
                                    "detail": [
                                        "Missing data for required field."
                                    ]
                                    }
                                },
                                "status": "fail"
                        })
#V6 - PATCH Disable my wallet
    elif request.is_ajax and request.method == "PATCH":
        if request.headers.get('Authorization') != None:
            token = request.headers["Authorization"]
            token = token.replace("Token ","")
            # Check query is_disabled == true
            is_disabled_body = request.body.decode('utf-8')
            return EnableWallet_PATCH(token,is_disabled_body)
        else:
            return JsonResponse( {
                                "data": {
                                    "error": {
                                    "customer_xid": [
                                        "Missing data for required field."
                                    ]
                                    }
                                },
                                "status": "fail"
                        })
    else:
        return JsonResponse({
                            "data": {
                                "detail": 'Not support this method'
                            },
                            "status": "error"       
        })

# V4- POST Add virtual money to my wallet
@csrf_exempt
def WalletDeposit(request):
    if request.is_ajax and request.method == "POST":
        if request.headers.get('Authorization') != None:
            # Get data from method POST
            token = request.headers["Authorization"]
            token = token.replace("Token ","")
            amount = request.POST.get('amount')
            reference_id = request.POST.get('reference_id')
            return WalletDeposit_POST(token, amount, reference_id)
        else:         
            return JsonResponse({
                                    "status": "fail",
                                    "data": {
                                        "error": "Token not found"
                                    }
                                })   
    return JsonResponse({
                         "data": {
                            "detail": 'Not support this method'
                        },
                        "status": "error"       
    })
# V5- POST Use virtual money from my wallet
@csrf_exempt
def UseMoney(request):
    if request.is_ajax and request.method == "POST":
        if request.headers.get('Authorization') != None:
            # Get data from method POST
            token = request.headers["Authorization"]
            token = token.replace("Token ","")
            amount = request.POST.get('amount')
            reference_id = request.POST.get('reference_id')
            return UserMoney_POST(token, amount, reference_id)
        else:
            return JsonResponse({
                                    "status": "fail",
                                    "data": {
                                        "error": "Token not found"
                                    }
                                })    
    else:
        return JsonResponse({
                            "data": {
                                "detail": 'Not support this method'
                            },
                            "status": "error"       
        })
