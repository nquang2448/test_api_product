from django.contrib import admin

from .models import Account, AccountWallet, Logging

class AccountAdmin(admin.ModelAdmin):
    list_display = ('customer_xid', 'token','status','id',)
    pass
class AccountWalletAdmin(admin.ModelAdmin):
    list_display = ('owned_by', 'balance','status','enabled_at','id',)
    pass
    
class LoggingAdmin(admin.ModelAdmin):
    list_display = ('owned_by', 'balance_current','balance_changed','reference_id','deposited_at',)
    pass
admin.site.register(Account, AccountAdmin)
admin.site.register(AccountWallet, AccountWalletAdmin)
admin.site.register(Logging, LoggingAdmin)
