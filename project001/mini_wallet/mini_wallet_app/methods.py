from .models import Account, AccountWallet, Logging
from django.http import JsonResponse
import json
import re
import binascii
import os
import datetime

# FUNCTION ---------------------------------------------------
# Function generate token
def generate_key():
    token_generate = binascii.hexlify(os.urandom(20)).decode()
    existed_token_check = Account.objects.filter(token=token_generate).count()
    # Check if had existed_token_check -> create new token
    while True:
        if existed_token_check > 0:
            # create new token if it's existed
            token_generate = binascii.hexlify(os.urandom(20)).decode()
            # check token again
            existed_token_check = Account.objects.filter(token=token_generate).count()
        else:
            break
    return token_generate

# Create account with POST method
def CreateMyAccount_POST(customer_xid,token):
    # Check customer_xid created or not 
    existed_customer_xid_check = Account.objects.filter(customer_xid=customer_xid).count()
    if existed_customer_xid_check == 0:
        created = Account.objects.create(
                    customer_xid = customer_xid,
                    token = token,
                )
        # Create new wallet for new account and status is disable
        CreateWalletForAccount(customer_xid)
        return JsonResponse( {
                        "data": {
                            "token": token
                        },
                        "status": "success"
                        })
    else:
        return JsonResponse( {
                        "data": {
                            "token": 'This account is created !'
                        },
                        "status": "fail"
                        })  

# # Create wallet for account function
def CreateWalletForAccount(customer_xid):
    customer_xid_id = Account.objects.get(customer_xid = customer_xid)
    created = AccountWallet.objects.create(
                owned_by = customer_xid_id,
                balance = 0,
            )

def EnableWallet_POST(token):
    # existed_wallet_check - Check wallet existed or not -> IF will not work
    existed_wallet_check = AccountWallet.objects.filter(owned_by__token = token).count()
    if existed_wallet_check == 1:
        authorization = AccountWallet.objects.get(owned_by__token = token)
        # check status wallet False -> True, If True _> response notice
        if authorization.status == False :
            today = datetime.datetime.now()
            # ACTIVE ----- update status and date time 
            authorization.status = True
            authorization.enabled_at = today
            authorization.save()
            return JsonResponse( {
                            "status": "success",
                            "data": {
                                "wallet": {
                                "id": authorization.id,
                                "owned_by": str(authorization).replace("\n",""),
                                "status": "enabled",
                                "enabled_at": today,
                                "balance": 0
                                }
                            }
                        })
        else:
            return JsonResponse( {
                                "status": "fail",
                                "data": {
                                    "error": "Already enabled"
                                }
                        })
    else :
        return JsonResponse( {
                        "data": {
                            "error": {
                            "customer_xid": [
                                "Account wallet not created !"
                            ]
                            }
                        },
                        "status": "fail"
                    })   
def EnableWallet_GET(token):
    # existed_wallet_check - Check wallet existed or not -> IF will not work
    existed_wallet_check = AccountWallet.objects.filter(owned_by__token = token).count()
    if existed_wallet_check == 1:
        authorization = AccountWallet.objects.get(owned_by__token = token) 
        # check status wallet False -> True, If True _> response notice
        if authorization.status == True :
            return JsonResponse( {
                            "status": "success",
                            "data": {
                                "wallet": {
                                "id": authorization.id,
                                "owned_by": str(authorization).replace("\n",""),
                                "status": "enabled",
                                "enabled_at": authorization.enabled_at,
                                "balance": authorization.balance,
                                }
                            }
                        }) 
        else:
            return JsonResponse( {
                            "status": "fail",
                            "data": {
                                "error": "Disabled"
                            }
                        })
    else:
        return JsonResponse( {
                        "status": "fail",
                        "data": {
                            "error": "Wallet is not created"
                        }
                    })
def EnableWallet_PATCH(token, is_disabled_body):
    # existed_wallet_check - Check wallet existed or not -> IF will not work
    existed_wallet_check = AccountWallet.objects.filter(owned_by__token = token).count()
    if existed_wallet_check == 1:
        authorization = AccountWallet.objects.get(owned_by__token = token) 
        # Check IF USER SEND TRUE -> DISABLE WALLET 
        today = datetime.datetime.now()
        if 'true' in str(is_disabled_body):
            authorization.status = False
            authorization.enabled_at = today
            authorization.save()
            return JsonResponse( {
                            "status": "success",
                            "data": {
                                "wallet": {
                                "id": authorization.id,
                                "owned_by": str(authorization).replace("\n",""),
                                "status": "disabled",
                                "disabled_at": today,
                                "balance": authorization.balance
                                }
                            }
                        })
        else:
            return JsonResponse( {
                            "status": "fail",
                            "data": {
                                "error": "Missing data for required field."
                            }
                        })
    else:
        return JsonResponse( {
                        "status": "fail",
                        "data": {
                            "error": "Wallet not created "
                        }
                    })   
    
def CalculateTheAmount(current_balance, amount,method):
    if method == "Deposit":
        result = int(current_balance) + int(amount)
        return result
    elif method == "Use":
        result = int(current_balance) - int(amount)
        return result

def WalletDeposit_POST(token,amount, reference_id):
    # existed_wallet_check - Check wallet existed or not -> IF will not work
    existed_wallet_check = AccountWallet.objects.filter(owned_by__token = token).count()
    if existed_wallet_check == 1:
        authorization = AccountWallet.objects.get(owned_by__token = token) 
        # check status wallet False -> True, If True _> response notice
        if authorization.status == True :
            # Check input value amount, reference_id
            if amount != None and reference_id != None:
                # Get data from method POST
                # amount = request.POST["amount"]
                if int(amount) >= 0 :
                    current_balance = authorization.balance
                    new_balance = CalculateTheAmount(amount,current_balance,"Deposit")
                    # reference_id = request.POST["reference_id"]
                    # Check if reference_id existed will not action :
                    existed_reference_id_check = Logging.objects.filter(reference_id = reference_id).count()
                    if existed_reference_id_check == 0:
                        today = datetime.datetime.now()
                        # update new balance in account wallet
                        authorization.balance = new_balance
                        authorization.save()
                        #create log of this transfer
                        logging = Logging(owned_by=authorization, balance_current= current_balance, balance_changed=amount,reference_id=reference_id,deposited_at=today)
                        logging.save()
                        return JsonResponse( {
                                        "status": "success",
                                        "data": {
                                            "deposit": {
                                            "id": authorization.id,
                                            "deposited_by": str(authorization).replace("\n",""),
                                            "status": "success",
                                            "deposited_at": today,
                                            "amount": amount,
                                            "reference_id": reference_id
                                            }
                                        }                                        
                                    })                              
                    else:
                        return JsonResponse( {
                                        "data": {
                                            "reference_id": 'This reference_id is created !'
                                        },
                                        "status": "fail"
                                        })
                else:
                    return JsonResponse( {
                                            "data": {
                                                "detail": 'The amount must be positive numbers'
                                            },
                                            "status": "fail"
                                })        
            else: 
                return JsonResponse( {
                                    "data": {
                                        "error": {
                                        "customer_xid": [
                                            "Missing data for required field."
                                        ]
                                        }
                                    },
                                    "status": "fail"
                            })                        

        else:
            return JsonResponse( {
                            "status": "fail",
                            "data": {
                                "error": "Disabled"
                            }
                        })  
    else:              
        return JsonResponse({
                                "status": "fail",
                                "data": {
                                    "error": "Wallet is not created"
                                }
                            })  


def UserMoney_POST(token, amount, reference_id):
    # existed_wallet_check - Check wallet existed or not -> IF will not work
    existed_wallet_check = AccountWallet.objects.filter(owned_by__token = token).count()
    if existed_wallet_check == 1:
        authorization = AccountWallet.objects.get(owned_by__token = token) 
        # check status wallet False -> True, If True _> response notice
        if authorization.status == True :
            # Check input value amount, reference_id
            if amount != None and reference_id != None:
                # Get data from method POST
                amount = amount
                if int(amount) >= 0 :
                    current_balance = authorization.balance
                    # Check current_balance always > amount withdrawals
                    if int(current_balance) >= int(amount) : 
                        new_balance = CalculateTheAmount(current_balance,amount,"Use")
                        # Check if reference_id existed will not action :
                        existed_reference_id_check = Logging.objects.filter(reference_id = reference_id).count()
                        if existed_reference_id_check == 0:
                            today = datetime.datetime.now()
                            # update new balance in account wallet
                            authorization.balance = new_balance
                            authorization.save()
                            #create log of this transfer
                            logging = Logging(owned_by=authorization, balance_current= current_balance, balance_changed=-int(amount),reference_id=reference_id,deposited_at=today)
                            logging.save()
                            return JsonResponse( {
                                            "status": "success",
                                            "data": {
                                                "withdrawal": {
                                                "id": authorization.id,
                                                "withdrawn_by": str(authorization).replace("\n",""),
                                                "status": "success",
                                                "withdrawn_at": today,
                                                "amount": amount,
                                                "reference_id": reference_id,
                                                }
                                            }
                                
                                        })                              
                        else:
                            return JsonResponse( {
                                            "data": {
                                                "reference_id": 'This reference_id is created !'
                                            },
                                            "status": "fail"
                                            })
                    else: 
                        return JsonResponse( {
                                                "data": {
                                                    "detail": 'The amount in the account wallet is not enough !'
                                                },
                                                "status": "fail"
                                    }) 
                else:
                    return JsonResponse( {
                                            "data": {
                                                "detail": 'The amount must be positive numbers'
                                            },
                                            "status": "fail"
                                })        
            else: 
                return JsonResponse( {
                                    "data": {
                                        "error": {
                                        "customer_xid": [
                                            "Missing data for required field."
                                        ]
                                        }
                                    },
                                    "status": "fail"
                            })                        

        else:
            return JsonResponse( {
                            "status": "fail",
                            "data": {
                                "error": "Disabled"
                            }
                        })                
    else:
        return JsonResponse({
                                "status": "fail",
                                "data": {
                                    "error": "Wallet is not created"
                                }
                            })   