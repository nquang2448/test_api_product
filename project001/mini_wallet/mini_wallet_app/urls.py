from django.urls import path,include
from . import views
from mini_wallet_app.views import (
    CreateMyAccount,
    EnableWallet,
    WalletDeposit,
    UseMoney,
)
urlpatterns = [
    path('v1/init/', CreateMyAccount, name = "v1_create_account"),
    path('v1/wallet/', EnableWallet, name = "v2_enable_wallet"),
    path('v1/wallet/deposits/', WalletDeposit, name = "v4_wallet_deposits"),
    path('v1/wallet/withdrawals/', UseMoney, name = "v5_use_money"),

]
